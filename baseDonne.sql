
Cours (id_cours , nom , image_cours, description , id_sujet); 

Sujet ( id_sujet, nom , description , image_sujet, id_exercice); 

Exercice ( id_exercice , nom , description ,image_exercie, reponse); 



create database plate_forme;
	use plate_forme; 


CREATE TABLE cours (
    id_cours int NOT NULL PRIMARY KEY,
    nom_cours varchar(255),
    image_cours varchar(255),
    description_cours varchar(255), 
    id_sujet int NOT NULL, 
    FOREIGN KEY (id_sujet) REFERENCES Sujet(id_sujet)
);


CREATE TABLE sujet (
    id_sujet int NOT NULL PRIMARY KEY,
    nom_sujet varchar(255),
    image_sujet varchar(255),
    description_cours varchar(255), 
    id_exercice int NOT NULL, 
    FOREIGN KEY (id_exercice) REFERENCES exercice(id_exercice)
);



CREATE TABLE exercice (
    id_exercice int NOT NULL PRIMARY KEY,
    nom_exercice varchar(255),
    image_exercice varchar(255),
    description_exercice varchar(255), 
    reponse varchar(255) NULL
);



CREATE TABLE user_test (
    id_user_test int NOT NULL PRIMARY KEY,
    nom_user_test varchar(255)  NOT NULL,
    password_user_test char(255) NOT NULL
);



ALTER TABLE  user_test MODIFY  id_user_test int  AUTO_INCREMENT ;
INSERT INTO user_test(nom_user_test,password_user_test) values ('admin','admin'); 





