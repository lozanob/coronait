<?php

namespace App\Entity;


use App\Repository\ClientRepository;
use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */
class Client{



    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="integer")
    */
    private $tel = '';



    /**
     * @ORM\Column(type="string", length=255)
    */
    private $mail = '';


    /**
     * @ORM\Column(type="string", length=255)
    */
    private $msg = ''; 



public function getId(): int
    {
        return $this->id;
    }


  public function getMail(): string
    {
        return $this->mail;
    }

    public function setMail(string $mail): void
    {
        $this->mail = $mail;
    }


 public function getmsg(): string
    {
        return $this->msg;
    }

    public function setMsg(string $msg): void
    {
        $this->msg = $msg;
    }




 public function getTel(): string
    {
        return $this->tel;
    }

    public function setTel(string $tel): void
    {
        $this->tel = $tel;
    }






}
