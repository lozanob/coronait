<?php 

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class support extends AbstractController
{


    public function support()
    {
        $number = random_int(0, 100);


        return $this->render('support.html.twig', ['number' => $number, ]);

    }



    
}