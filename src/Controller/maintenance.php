<?php 

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class maintenance extends AbstractController
{

	
    public function maintenance()
    {
        $number = random_int(0, 100);


          return $this->render('maintenance.html.twig', ['number' => $number, ]);

        //return new Response(
          //  '<html><body>Lucky number: '.$number.'</body></html>'
        //);
    }
}