<?php 

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class evolutions extends AbstractController
{
    public function evolutions()
    {
        $number = random_int(0, 100);

        return $this->render('evolutions.html.twig', ['number' => $number, ]);
        //return new Response(
          //  '<html><body>Lucky number: '.$number.'</body></html>'
        //)
    }

    
}